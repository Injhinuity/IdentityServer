﻿using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using IdentityServer4.Models;

namespace AspNetCoreIdentityServer.Configurations
{
    public class Config
    {
        public static string CredsFileName { get; } = "./Creds/credentials.json";

        public static IEnumerable<ApiResource> GetApiResources()
        {
            //List of Apis we want to expose in our IdentityServer
            return new List<ApiResource>
            {
                new ApiResource("InjhinuityApi", "InjhinuityApi")
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            //Fetches the credentials file and extracts the data
            var configBuilder = new ConfigurationBuilder()
                    .AddJsonFile(CredsFileName);

            var data = configBuilder.Build();

            //Returns a new client that we'll use the authenticate from the client
            return new List<Client>
            {
                new Client
                {
                    ClientId = data["ApiClientId"],
                    AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,
                    ClientSecrets =
                    {
                        new Secret(data["ApiClientSecret"].Sha256())
                    },
                    AllowedScopes =
                    {
                        "InjhinuityApi"
                    },
                }
            };
        }
    }
}