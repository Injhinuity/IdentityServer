# IdentityServer
Simple implementation of a Web App running the Identity Server 4 library. Authenticates client applications for data consumption of a [RESTful API](https://github.com/MathieuPomerleau/JhinBotAPI).

## What does it do?
Using a client secret, it authenticates and grants access to desired parts of our API for data consumption. Since only a single client currently consumes the API, only one section needs to be authorized. As more sections are added, sections will be split and authentication will be handled individually on a client to client basis.
